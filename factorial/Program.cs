﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Diagnostics;
using System.Text;
using System.Threading.Tasks;

namespace factorial
{
    class Program
    {
        static void Main(string[] args)
        {
           
            int numbers;
            int fact = 1;
            int i;
            Console.Write("Enter the number: ");
            numbers = Convert.ToInt32(Console.ReadLine());

            for (i = 1; i <=numbers; i++)
            {
                fact = i * fact;
            }
            Console.WriteLine("The factorial of "+numbers +" is: "+fact);
            Console.ReadKey();
        }
    }
}
